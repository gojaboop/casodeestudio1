<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'bookstore' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@^l@P;ZAH<2.cq3Uhb^PN6&~c:c4&]9wc>,/&*rAKcN%BS|O$i(jW1AIh=;&(DR(' );
define( 'SECURE_AUTH_KEY',  'dNld^c8i>7xCuz?G<{ueH8_vx%gAb!I=He&5mWi*g<eV8LKXp6m8R/|kC37(*@=.' );
define( 'LOGGED_IN_KEY',    's@ 4r|iAjyb#lfgw rYK9Yo?<X&8-I:zhC9Z|RB3h>XsgZ;Dg;U,amIWg~pQ2W0(' );
define( 'NONCE_KEY',        '/8L U>:%BVk&s[O/,-qzVh#8P^P8srD5Y[ M2a]/Gb= ;eOHs6xXYg1FHQu%KHnJ' );
define( 'AUTH_SALT',        '/&oYiY-?@FQQu*`^wm,9L}VqM~6.BQY~1K^SnEBF9LG]aYnn7;B_gy>Z8FIYUj`s' );
define( 'SECURE_AUTH_SALT', '#Ouu&Xe(KlK*X$yu]>9&Grw <x)cYhc Eq:n*R^F&fnj->k|tSLmpCM~Qw|4+vXj' );
define( 'LOGGED_IN_SALT',   '-/DwL{7,o>=?)O;@IoB_o]F(s d<235eWwRaR@2[J-ff{lo%PfIDv)6/=^g!N{{J' );
define( 'NONCE_SALT',       'C=o|#)^Zh~uK9=*Mcqa:o^d5[cAda*ihbpOQ dFgeqx.+N87lOk,#29DBxZOLK^j' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
